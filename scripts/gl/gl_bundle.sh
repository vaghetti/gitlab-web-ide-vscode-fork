#!/usr/bin/env bash

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

BUNDLE_NAME=vscode-web-$(cat FULL_VERSION)

mkdir .build/vscode-web-dist
tar -czvf ".build/vscode-web-dist/${BUNDLE_NAME}.tar.gz" -C .build/vscode-web .
