#!/usr/bin/env bash

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
# xtrace is commented out to reduce the script noise on local machine
# set -o xtrace # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

if [ "$1" = "dev" ]; then
	version="$(cat VERSION)-dev-$(TZ=UTC date '+%Y%m%d%H%M%S')"
fi

if [ "$1" = "prod" ]; then
	version="$(cat VERSION)"
fi

if [ -z "${version}" ]; then
	echo "please use 'dev' or 'prod' as an argument"
	exit 1
fi

git tag "${version}"
echo "Tag '${version}' has been successfully created. You can push it now."
