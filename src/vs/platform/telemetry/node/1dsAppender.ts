/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *  Licensed under the MIT License. See License.txt in the project root for license information.
 *--------------------------------------------------------------------------------------------*/

// gl_mod START let's remove need for 1ds
import { NoopAppender } from 'vs/platform/telemetry/common/gitlab/noopAppender';

export {
	NoopAppender as OneDataSystemAppender
};
// gl_mod END
