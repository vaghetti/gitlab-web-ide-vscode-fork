import { Disposable } from 'vs/base/common/lifecycle';
import { InstantiationType, registerSingleton } from 'vs/platform/instantiation/common/extensions';
import { IConfigurationService } from 'vs/platform/configuration/common/configuration';
import { ClassifiedEvent, OmitMetadata, IGDPRProperty, StrictPropertyCheck } from 'vs/platform/telemetry/common/gdprTypings';
import { ITelemetryData, ITelemetryInfo, ITelemetryService, TelemetryLevel, TELEMETRY_SETTING_ID } from 'vs/platform/telemetry/common/telemetry';
import { getTelemetryLevel } from 'vs/platform/telemetry/common/telemetryUtils';

interface VSCodeTrackingEvent {
	name: string;
	data?: unknown;
}

const postVSCodeTrackingMessage = (event: VSCodeTrackingEvent) => {
	window.postMessage({ key: 'vscode-tracking', params: { event } }, window.location.origin);
};

export class GitLabTelemetryService extends Disposable implements ITelemetryService {
	declare readonly _serviceBrand: undefined;
	readonly sendErrorTelemetry = false;
	telemetryLevel;

	constructor(@IConfigurationService configurationService: IConfigurationService) {
		super();

		this.telemetryLevel = getTelemetryLevel(configurationService);

		// When the level changes it could change from off to on and we want to make sure telemetry is properly intialized
		this._register(configurationService.onDidChangeConfiguration(e => {
			if (e.affectsConfiguration(TELEMETRY_SETTING_ID)) {
				this.telemetryLevel = getTelemetryLevel(configurationService);
			}
		}));
	}

	async publicLog(eventName: string, data?: ITelemetryData) {
		if (this.telemetryLevel >= TelemetryLevel.USAGE) {
			postVSCodeTrackingMessage({ name: eventName, data });
		}
	}
	publicLog2<E extends ClassifiedEvent<OmitMetadata<T>> = never, T extends IGDPRProperty = never>(eventName: string, data?: StrictPropertyCheck<T, E>) {
		return this.publicLog(eventName, data as ITelemetryData);
	}
	publicLogError(eventName: string, data?: ITelemetryData) {
		if (this.telemetryLevel >= TelemetryLevel.ERROR) {
			postVSCodeTrackingMessage({ name: eventName, data });
		}

		return Promise.resolve(undefined);
	}
	publicLogError2<E extends ClassifiedEvent<OmitMetadata<T>> = never, T extends IGDPRProperty = never>(eventName: string, data?: StrictPropertyCheck<T, E>) {
		return this.publicLogError(eventName, data as ITelemetryData);
	}

	setExperimentProperty() { }

	async getTelemetryInfo(): Promise<ITelemetryInfo> {
		return {
			sessionId: 'someValue.sessionId',
			machineId: 'someValue.machineId',
			firstSessionDate: 'someValue.firstSessionDate'
		};
	}
}

registerSingleton(ITelemetryService, GitLabTelemetryService, InstantiationType.Delayed);
